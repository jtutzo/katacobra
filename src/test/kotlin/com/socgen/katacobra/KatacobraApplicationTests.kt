package com.socgen.katacobra

import com.socgen.katacobra.api.resource.TaskReadResource
import com.socgen.katacobra.api.resource.TeamWriteResource
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class KatacobraApplicationTests {

    @Autowired
    private lateinit var taskReadResource: TaskReadResource

    @Autowired
    private lateinit var taskWriteResource: TaskReadResource

    @Autowired
    private lateinit var teamReadResource: TaskReadResource

    @Autowired
    private lateinit var teamWriteResource: TeamWriteResource

    @Autowired
    private lateinit var userReadResource: TaskReadResource

    @Autowired
    private lateinit var userWriteResource: TeamWriteResource

    @Test
    fun contextLoads() {
        assertThat(taskReadResource).isNotNull
        assertThat(taskWriteResource).isNotNull
        assertThat(teamReadResource).isNotNull
        assertThat(teamWriteResource).isNotNull
        assertThat(userReadResource).isNotNull
        assertThat(userWriteResource).isNotNull
    }

}
