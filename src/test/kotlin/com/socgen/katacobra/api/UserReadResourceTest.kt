package com.socgen.katacobra.api

import com.nhaarman.mockitokotlin2.any
import com.socgen.katacobra.domaine.user.UserReadRepository
import com.socgen.katacobra.domaine.user.UserWriteRepository
import com.socgen.katacobra.util.DataTest.tasks
import com.socgen.katacobra.util.DataTest.users
import com.socgen.katacobra.util.buildToJson
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class UserReadResourceTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var userReadRepository: UserReadRepository

    @MockBean
    private lateinit var userWriteRepository: UserWriteRepository

    private val url = "/users"

    @Test
    fun `should 200 when get all task`() {
        // Then
        `when`(userReadRepository.findAll()).thenReturn(users)

        // When / Then
        mockMvc.perform(get(url))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(users.buildToJson()))
    }

    @Test
    fun `should 200 when get by id`() {
        // Then
        `when`(userReadRepository.findById(any())).thenReturn(Optional.of(users.first()))

        // When / Then
        mockMvc.perform(get("$url/${UUID.randomUUID()}"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(users.first().buildToJson()))
    }

    @Test
    fun `should 404 when no user match with id`() {
        // Then
        `when`(userReadRepository.findById(any())).thenReturn(Optional.empty())

        // When / Then
        mockMvc.perform(get("$url/${UUID.randomUUID()}"))
                .andExpect(status().isNotFound)
    }

}