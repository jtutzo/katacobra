package com.socgen.katacobra.api

import com.nhaarman.mockitokotlin2.any
import com.socgen.katacobra.domaine.task.TaskReadRepository
import com.socgen.katacobra.domaine.task.TaskWriteRepository
import com.socgen.katacobra.util.DataTest.createTaskDTO
import com.socgen.katacobra.util.DataTest.tasks
import com.socgen.katacobra.util.DataTest.updateTaskDTO
import com.socgen.katacobra.util.buildToJson
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class TaskWriteResourceTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var taskReadRepository: TaskReadRepository

    @MockBean
    private lateinit var taskWriteRepository: TaskWriteRepository

    private val url = "/tasks"

    @Test
    fun `should 201 when create a task`() {
        // Then
        `when`(taskReadRepository.findByLabel(createTaskDTO.label)).thenReturn(Optional.empty())
        doNothing().`when`(taskWriteRepository).create(any())

        // When / Then
        mockMvc.perform(post(url)
                .content(createTaskDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated)
    }

    @Test
    fun `shouldn't 409 when create a task with label already used`() {
        // Then
        `when`(taskReadRepository.findByLabel(createTaskDTO.label)).thenReturn(Optional.of(tasks.first()))
        doNothing().`when`(taskWriteRepository).create(any())

        // When / Then
        mockMvc.perform(post(url)
                .content(createTaskDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict)
    }

    @Test
    fun `should 200 when update a task`() {
        // Then
        `when`(taskReadRepository.findByLabel(updateTaskDTO.label)).thenReturn(Optional.empty())
        doNothing().`when`(taskWriteRepository).update(any())

        // When / Then
        mockMvc.perform(put("$url/${UUID.randomUUID()}")
                .content(updateTaskDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
    }

    @Test
    fun `shouldn't 409 when update a task with label already used`() {
        // Then
        `when`(taskReadRepository.findByLabel(updateTaskDTO.label)).thenReturn(Optional.of(tasks.elementAt(1)))
        doNothing().`when`(taskWriteRepository).update(any())

        // When / Then
        mockMvc.perform(put("$url/${UUID.randomUUID()}")
                .content(updateTaskDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict)
    }

}