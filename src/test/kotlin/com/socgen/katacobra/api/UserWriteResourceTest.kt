package com.socgen.katacobra.api

import com.nhaarman.mockitokotlin2.any
import com.socgen.katacobra.domaine.user.UserReadRepository
import com.socgen.katacobra.domaine.user.UserWriteRepository
import com.socgen.katacobra.util.DataTest.createUserDTO
import com.socgen.katacobra.util.DataTest.updateUserDTO
import com.socgen.katacobra.util.DataTest.users
import com.socgen.katacobra.util.buildToJson
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class UserWriteResourceTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var userReadRepository: UserReadRepository

    @MockBean
    private lateinit var userWriteRepository: UserWriteRepository

    private val url = "/users"

    @Test
    fun `should 201 when create an user`() {
        // Then
        `when`(userReadRepository.findByUsername(createUserDTO.username)).thenReturn(Optional.empty())
        doNothing().`when`(userWriteRepository).create(any())

        // When / Then
        mockMvc.perform(post(url)
                .content(createUserDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated)
    }

    @Test
    fun `should 409 when create an user with username already used`() {
        // Then
        `when`(userReadRepository.findByUsername(createUserDTO.username)).thenReturn(Optional.of(users.first()))
        doNothing().`when`(userWriteRepository).create(any())

        // When / Then
        mockMvc.perform(post(url)
                .content(createUserDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict)
    }

    @Test
    fun `should 200 when update an user`() {
        // Then
        `when`(userReadRepository.findByUsername(updateUserDTO.username)).thenReturn(Optional.empty())
        doNothing().`when`(userWriteRepository).update(any())

        // When / Then
        mockMvc.perform(put("$url/${UUID.randomUUID()}")
                .content(updateUserDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
    }

    @Test
    fun `should 409 when update an user with username already used`() {
        // Then
        `when`(userReadRepository.findByUsername(updateUserDTO.username)).thenReturn(Optional.of(users.elementAt(1)))
        doNothing().`when`(userWriteRepository).update(any())

        // When / Then
        mockMvc.perform(put("$url/${UUID.randomUUID()}")
                .content(updateUserDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict)
    }

}