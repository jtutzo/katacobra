package com.socgen.katacobra.api

import com.nhaarman.mockitokotlin2.any
import com.socgen.katacobra.domaine.team.TeamReadRepository
import com.socgen.katacobra.domaine.team.TeamWriteRepository
import com.socgen.katacobra.util.DataTest.buildTeam
import com.socgen.katacobra.util.DataTest.createTeamDTO
import com.socgen.katacobra.util.DataTest.teams
import com.socgen.katacobra.util.DataTest.updateTaskDTO
import com.socgen.katacobra.util.DataTest.updateTeamDTO
import com.socgen.katacobra.util.buildToJson
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class TeamWriteResourceTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var teamReadRepository: TeamReadRepository

    @MockBean
    private lateinit var teamWriteRepository: TeamWriteRepository

    private val url = "/teams"

    @Test
    fun `should 201 when create a team`() {
        // Then
        `when`(teamReadRepository.findByLabel(createTeamDTO.label)).thenReturn(Optional.empty())
        doNothing().`when`(teamWriteRepository).create(any())

        // When / Then
        mockMvc.perform(post(url)
                .content(createTeamDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated)
    }

    @Test
    fun `should 409 when create a team with label already used`() {
        // Then
        `when`(teamReadRepository.findByLabel(createTeamDTO.label)).thenReturn(Optional.of(teams.first()))
        doNothing().`when`(teamWriteRepository).create(any())

        // When / Then
        mockMvc.perform(post(url)
                .content(createTeamDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict)
    }

    @Test
    fun `should 200 when update a team`() {
        // Then
        `when`(teamReadRepository.findByLabel(updateTeamDTO.label)).thenReturn(Optional.empty())
        doNothing().`when`(teamWriteRepository).update(any())

        // When / Then
        mockMvc.perform(put("$url/${UUID.randomUUID()}")
                .content(updateTeamDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
    }

    @Test
    fun `should 409 when update a team with label already used`() {
        // Then
        `when`(teamReadRepository.findByLabel(updateTeamDTO.label)).thenReturn(Optional.of(teams.elementAt(1)))
        doNothing().`when`(teamWriteRepository).update(any())

        // When / Then
        mockMvc.perform(put("$url/${UUID.randomUUID()}")
                .content(updateTeamDTO.buildToJson())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict)
    }

}