package com.socgen.katacobra.repository

import com.socgen.katacobra.domaine.UserEntity
import com.socgen.katacobra.domaine.team.TeamWriteRepository
import com.socgen.katacobra.domaine.user.UserReadRepository
import com.socgen.katacobra.domaine.user.UserWriteRepository
import com.socgen.katacobra.util.DataTest.buildUser
import com.socgen.katacobra.util.DataTest.teams
import com.socgen.katacobra.util.DataTest.users
import com.socgen.katacobra.util.build
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class UserRepositoryTest {

    @Autowired
    private lateinit var userReadRepository: UserReadRepository

    @Autowired
    private lateinit var userWriteRepository: UserWriteRepository

    @Autowired
    private lateinit var teamWriteRepository: TeamWriteRepository


    @Before
    fun init() {
        teams.map { teamWriteRepository.create(it) }
        users.map { userWriteRepository.create(it) }
    }

    @After
    fun deleteAll() {
        userWriteRepository.deleteAll()
        teamWriteRepository.deleteAll()
    }

    @Test
    fun `should get all user`() {
        // When
        val usersFound = userReadRepository.findAll()

        // Then
        assertThat(usersFound).isEqualTo(users)
    }

    @Test
    fun `should get empty user when database is empty`() {
        // Given
        userWriteRepository.deleteAll()

        // When
        val usersFound = userReadRepository.findAll()

        // Then
        assertThat(usersFound).isEqualTo(emptySet<UserEntity>())
    }

    @Test
    fun `should get user optional when id match`() {
        // When
        val userFound = userReadRepository.findById(users.first().id)

        // Then
        assertThat(userFound).isEqualTo(Optional.of(users.first()))
    }

    @Test
    fun `should get empty optional when id no match`() {
        // When
        val userFound = userReadRepository.findById(UUID.randomUUID())

        // Then
        assertThat(userFound).isEqualTo(Optional.empty<UserEntity>())
    }

    @Test
    fun `should get user optional when label match`() {
        // When
        val userFound = userReadRepository.findByUsername(users.first().username)

        // Then
        assertThat(userFound).isEqualTo(Optional.of(users.first()))
    }

    @Test
    fun `should get empty optional when label no match`() {
        // Then
        val userFound = userReadRepository.findByUsername("Search by username")

        // When
        assertThat(userFound).isEqualTo(Optional.empty<UserEntity>())
    }

    @Test
    fun `should create a user`() {
        // Given
        val user = buildUser(username = "create user", teamId = teams.first().id)

        // When
        userWriteRepository.create(user)

        // Then
        val userFound = userReadRepository.findById(user.id)
        assertThat(userFound).isEqualTo(Optional.of(user))
    }

    @Test
    fun `should update a user`() {
        // Given
        val userUpdate = users.first().build(username = "update user")

        // When
        userWriteRepository.update(userUpdate)

        // Then
        val userFound = userReadRepository.findById(userUpdate.id)
        assertThat(userFound).isEqualTo(Optional.of(userUpdate))
    }

    @Test
    fun `should delete all user`() {
        // When
        userWriteRepository.deleteAll()

        // Then
        val usersFound = userReadRepository.findAll()
        assertThat(usersFound).isEqualTo(emptySet<UserEntity>())
    }

}