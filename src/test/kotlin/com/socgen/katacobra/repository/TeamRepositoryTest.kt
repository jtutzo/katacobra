package com.socgen.katacobra.repository

import com.socgen.katacobra.domaine.TeamEntity
import com.socgen.katacobra.domaine.team.TeamReadRepository
import com.socgen.katacobra.domaine.team.TeamWriteRepository
import com.socgen.katacobra.util.DataTest.buildTeam
import com.socgen.katacobra.util.DataTest.teams
import com.socgen.katacobra.util.build
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class TeamRepositoryTest {

    @Autowired
    private lateinit var teamReadRepository: TeamReadRepository

    @Autowired
    private lateinit var teamWriteRepository: TeamWriteRepository

    @Before
    fun init() {
        teams.map { teamWriteRepository.create(it) }
    }

    @After
    fun deleteAll() {
        teamWriteRepository.deleteAll()
    }

    @Test
    fun `should get all team`() {
        // When
        val teamsFound = teamReadRepository.findAll()

        // Then
        assertThat(teamsFound).isEqualTo(teams)
    }

    @Test
    fun `should get empty team when database is empty`() {
        // Given
        teamWriteRepository.deleteAll()

        // When
        val teamsFound = teamReadRepository.findAll()

        // Then
        assertThat(teamsFound).isEqualTo(emptySet<TeamEntity>())
    }

    @Test
    fun `should get team optional when id match`() {
        // When
        val teamFound = teamReadRepository.findById(teams.first().id)

        // Then
        assertThat(teamFound).isEqualTo(Optional.of(teams.first()))
    }

    @Test
    fun `should get empty optional when id no match`() {
        // When
        val teamFound = teamReadRepository.findById(UUID.randomUUID())

        // Then
        assertThat(teamFound).isEqualTo(Optional.empty<TeamEntity>())
    }

    @Test
    fun `should get team optional when label match`() {
        // When
        val teamFound = teamReadRepository.findByLabel(teams.first().label)

        // Then
        assertThat(teamFound).isEqualTo(Optional.of(teams.first()))
    }

    @Test
    fun `should get empty optional when label no match`() {
        // Then
        val teamFound = teamReadRepository.findByLabel("Search by label")

        // When
        assertThat(teamFound).isEqualTo(Optional.empty<TeamEntity>())
    }

    @Test
    fun `should create a team`() {
        // Given
        val team = buildTeam(label = "create team")

        // When
        teamWriteRepository.create(team)

        // Then
        val teamFound = teamReadRepository.findById(team.id)
        assertThat(teamFound).isEqualTo(Optional.of(team))
    }

    @Test
    fun `should update a team`() {
        // Given
        val teamUpdate = teams.first().build(label = "update team")

        // When
        teamWriteRepository.update(teamUpdate)

        // Then
        val teamFound = teamReadRepository.findById(teamUpdate.id)
        assertThat(teamFound).isEqualTo(Optional.of(teamUpdate))
    }

    @Test
    fun `should delete all team`() {
        // When
        teamWriteRepository.deleteAll()

        // Then
        val teamsFound = teamReadRepository.findAll()
        assertThat(teamsFound).isEqualTo(emptySet<TeamEntity>())
    }

}