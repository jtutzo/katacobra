package com.socgen.katacobra.repository

import com.socgen.katacobra.domaine.TaskEntity
import com.socgen.katacobra.domaine.task.TaskReadRepository
import com.socgen.katacobra.domaine.task.TaskWriteRepository
import com.socgen.katacobra.domaine.team.TeamWriteRepository
import com.socgen.katacobra.domaine.user.UserWriteRepository
import com.socgen.katacobra.util.DataTest.buildTask
import com.socgen.katacobra.util.DataTest.tasks
import com.socgen.katacobra.util.DataTest.teams
import com.socgen.katacobra.util.DataTest.users
import com.socgen.katacobra.util.build
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class TaskRepositoryTest {

    @Autowired
    private lateinit var taskReadRepository: TaskReadRepository

    @Autowired
    private lateinit var taskWriteRepository: TaskWriteRepository

    @Autowired
    private lateinit var teamWriteRepository: TeamWriteRepository

    @Autowired
    private lateinit var userWriteRepository: UserWriteRepository

    @Before
    fun init() {
        teams.map { teamWriteRepository.create(it) }
        users.map { userWriteRepository.create(it) }
        tasks.map { taskWriteRepository.create(it) }
    }

    @After
    fun deleteAll() {
        taskWriteRepository.deleteAll()
        userWriteRepository.deleteAll()
        teamWriteRepository.deleteAll()
    }

    @Test
    fun `should get all task`() {
        // When
        val tasksFound = taskReadRepository.findAll()

        // Then
        assertThat(tasksFound).isEqualTo(tasks)
    }

    @Test
    fun `should get empty task when database is empty`() {
        // Given
        taskWriteRepository.deleteAll()

        // When
        val tasksFound = taskReadRepository.findAll()

        // Then
        assertThat(tasksFound).isEqualTo(emptySet<TaskEntity>())
    }

    @Test
    fun `should get task optional when id match`() {
        // When
        val taskFound = taskReadRepository.findById(tasks.first().id)

        // Then
        assertThat(taskFound).isEqualTo(Optional.of(tasks.first()))
    }

    @Test
    fun `should get empty optional when id no match`() {
        // When
        val taskFound = taskReadRepository.findById(UUID.randomUUID())

        // Then
        assertThat(taskFound).isEqualTo(Optional.empty<TaskEntity>())
    }

    @Test
    fun `should get task optional when label match`() {
        // When
        val taskFound = taskReadRepository.findByLabel(tasks.first().label)

        // Then
        assertThat(taskFound).isEqualTo(Optional.of(tasks.first()))
    }

    @Test
    fun `should get empty optional when label no match`() {
        // Then
        val taskFound = taskReadRepository.findByLabel("Search by label")

        // When
        assertThat(taskFound).isEqualTo(Optional.empty<TaskEntity>())
    }

    @Test
    fun `should create a task`() {
        // Given
        val task = buildTask(label = "create task")

        // When
        taskWriteRepository.create(task)

        // Then
        val taskFound = taskReadRepository.findById(task.id)
        assertThat(taskFound).isEqualTo(Optional.of(task))
    }

    @Test
    fun `should update a task`() {
        // Given
        val taskUpdate = tasks.first().build(label = "update task",
                users = setOf(users.elementAt(2).id, users.elementAt(3).id),
                teams = setOf(teams.elementAt(3).id, teams.elementAt(2).id))

        // When
        taskWriteRepository.update(taskUpdate)

        // Then
        val taskFound = taskReadRepository.findById(taskUpdate.id)
        assertThat(taskFound).isEqualTo(Optional.of(taskUpdate))
    }

    @Test
    fun `should delete all task`() {
        // When
        taskWriteRepository.deleteAll()

        // Then
        val tasksFound = taskReadRepository.findAll()
        assertThat(tasksFound).isEqualTo(emptySet<TaskEntity>())
    }

}