package com.socgen.katacobra.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.socgen.katacobra.domaine.TaskEntity
import com.socgen.katacobra.domaine.TeamEntity
import com.socgen.katacobra.domaine.TeamType
import com.socgen.katacobra.domaine.UserEntity
import java.util.*

fun TeamEntity.build(id: UUID = this.id,
                     supervisorId: UUID? = this.supervisorId,
                     label: String = this.label,
                     teamType: TeamType = this.teamType) = TeamEntity(id, supervisorId, label, teamType)

fun TaskEntity.build(id: UUID = this.id,
                     label: String = this.label,
                     users: Set<UUID> = this.users,
                     teams: Set<UUID> = this.teams) = TaskEntity(id, label, users, teams)

fun UserEntity.build(id: UUID = this.id, username: String = this.username, teamId: UUID = this.teamId) =
        UserEntity(id, username, teamId)

fun Any.buildToJson() = ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this)
