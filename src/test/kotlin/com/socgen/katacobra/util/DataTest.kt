package com.socgen.katacobra.util

import com.socgen.katacobra.api.*
import com.socgen.katacobra.domaine.TaskEntity
import com.socgen.katacobra.domaine.TeamEntity
import com.socgen.katacobra.domaine.TeamType
import com.socgen.katacobra.domaine.UserEntity
import java.util.*

object DataTest {

    val teams = setOf<TeamEntity>()
            .plus(buildTeam(label = "team1"))
            .plus(buildTeam(label = "team2"))
            .plus(buildTeam(label = "team3"))
            .plus(buildTeam(label = "team4"))

    val users = setOf<UserEntity>()
            .plus(buildUser(username = "user1", teamId = teams.first().id))
            .plus(buildUser(username = "user2", teamId = teams.elementAt(1).id))
            .plus(buildUser(username = "user3", teamId = teams.elementAt(1).id))
            .plus(buildUser(username = "user4", teamId = teams.elementAt(3).id))

    val tasks = setOf<TaskEntity>()
            .plus(buildTask(label = "task1",
                    users = setOf(users.first().id, users.elementAt(1).id),
                    teams = setOf(teams.first().id, teams.elementAt(1).id)))
            .plus(buildTask(label = "task2",
                    users = setOf(users.elementAt(2).id, users.elementAt(3).id),
                    teams = setOf(teams.elementAt(2).id, teams.elementAt(3).id)))
            .plus(buildTask(label = "task3"))
            .plus(buildTask(label = "task4"))

    val createTeamDTO = teams.first().let { CreateTeamDTO(it.label, it.teamType, it.supervisorId) }

    val updateTeamDTO = teams.elementAt(1).let { UpdateTeamDTO(it.label, it.supervisorId) }

    val createTaskDTO = CreateTaskDTO(tasks.first().label)

    val updateTaskDTO = UpdateTaskDTO(tasks.elementAt(1).label)

    val createUserDTO = users.first().let { CreateUserDTO(it.username, it.teamId) }

    val updateUserDTO = users.elementAt(1).let { UpdateUserDTO(it.username, it.teamId) }

    fun buildTeam(id: UUID = UUID.randomUUID(),
                  supervisorId: UUID? = null,
                  label: String = "team",
                  teamType: TeamType = TeamType.DEFAULT) =
            TeamEntity(id, supervisorId, label, teamType)

    fun buildTask(id: UUID = UUID.randomUUID(),
                  label: String = "task",
                  users: Set<UUID> = emptySet(),
                  teams: Set<UUID> = emptySet()) =
            TaskEntity(id, label, users, teams)

    fun buildUser(id: UUID = UUID.randomUUID(), username: String = "user", teamId: UUID) =
            UserEntity(id, username, teamId)

}

