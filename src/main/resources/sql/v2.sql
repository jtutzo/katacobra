DROP TABLE IF EXISTS group;
DROP TABLE IF EXISTS team_group;
DROP TABLE IF EXISTS task_group;

CREATE TABLE group (
  id UUID NOT NULL,
  name VARCHAR(50)

  CONSTRAINT pk_t_group PRIMARY KEY (ID)
);

CREATE TABLE team_group (
  id UUID NOT NULL,
  team_id UUID NOT NULL,
  group_id UUID NOT NULL,

  CONSTRAINT pk_t_team_group PRIMARY KEY (ID),
  CONSTRAINT fk_t_team_group_team_id FOREIGN KEY (team_id) REFERENCES team(id),
  CONSTRAINT fk_t_team_group_group_id FOREIGN KEY (group_id) REFERENCES group(id)
);

CREATE TABLE task_group (
  id UUID NOT NULL,
  task_id UUID NOT NULL,
  group_id UUID NOT NULL,

  CONSTRAINT pk_t_task_group PRIMARY KEY (ID),
  CONSTRAINT fk_t_task_group_task_id FOREIGN KEY (task_id) REFERENCES task(id),
  CONSTRAINT fk_t_task_group_group_id FOREIGN KEY (group_id) REFERENCES group(id)
);
