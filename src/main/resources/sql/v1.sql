DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS team_overseen_team;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS task_team;
DROP TABLE IF EXISTS task_user;

CREATE TABLE user (
  id UUID NOT NULL,
  username VARCHAR(50) NOT NULL,
  team_id UUID NOT NULL,

  CONSTRAINT pk_t_user PRIMARY KEY (ID)
);

CREATE TABLE team (
  id UUID NOT NULL,
  supervisor_id UUID,
  label VARCHAR(50) NOT NULL,
  team_type VARCHAR(50) NOT NULL,

  CONSTRAINT pk_t_team PRIMARY KEY (ID)
);

ALTER TABLE user
ADD CONSTRAINT fk_t_user_team_id FOREIGN KEY (team_id) REFERENCES team(id);

ALTER TABLE team
ADD CONSTRAINT fk_t_team_supervisor_id FOREIGN KEY (supervisor_id) REFERENCES user(id);

CREATE TABLE team_overseen_team (
  id UUID NOT NULL,
  team_id UUID NOT NULL,
  overseen_team_id UUID NOT NULL,

  CONSTRAINT pk_t_team_overseen_team PRIMARY KEY (ID),
  CONSTRAINT fk_t_team_overseen_team_team_id FOREIGN KEY (team_id) REFERENCES user(id),
  CONSTRAINT fk_t_team_overseen_team_overseen_team_id FOREIGN KEY (overseen_team_id) REFERENCES team(id)
);

CREATE TABLE task (
  id UUID NOT NULL,
  label VARCHAR(50) NOT NULL,

  CONSTRAINT pk_t_task PRIMARY KEY (ID)
);

CREATE TABLE task_team (
  id UUID NOT NULL,
  task_id UUID NOT NULL,
  team_id UUID NOT NULL,

  CONSTRAINT pk_t_task_team PRIMARY KEY (ID),
  CONSTRAINT fk_t_task_team_task_id FOREIGN KEY (task_id) REFERENCES task(id),
  CONSTRAINT fk_t_task_team_team_id FOREIGN KEY (team_id) REFERENCES team(id)
);

CREATE TABLE task_user (
  id UUID NOT NULL,
  task_id UUID NOT NULL,
  user_id UUID NOT NULL,

  CONSTRAINT pk_t_task_user PRIMARY KEY (ID),
  CONSTRAINT fk_t_task_user_task_id FOREIGN KEY (task_id) REFERENCES task(id),
  CONSTRAINT fk_t_task_user_user_id FOREIGN KEY (user_id) REFERENCES user(id)
);
