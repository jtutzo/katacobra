package com.socgen.katacobra.api

import com.socgen.katacobra.domaine.TeamType
import org.springframework.lang.NonNull
import java.util.*

interface Dto

data class CreateTaskDTO(@NonNull val label: String,
                         val users: Set<UUID> = emptySet(),
                         val teams: Set<UUID> = emptySet()) : Dto

data class UpdateTaskDTO(@NonNull val label: String,
                         val users: Set<UUID> = emptySet(),
                         val teams: Set<UUID> = emptySet()) : Dto

data class CreateTeamDTO(@NonNull val label: String,
                         @NonNull val teamType: TeamType,
                         val supervisorId: UUID? = null) : Dto

data class UpdateTeamDTO(@NonNull val label: String,
                         val supervisorId: UUID? = null) : Dto

data class CreateUserDTO(@NonNull val username: String,
                         @NonNull val teamId: UUID) : Dto

data class UpdateUserDTO(@NonNull val username: String,
                         @NonNull val teamId: UUID) : Dto


