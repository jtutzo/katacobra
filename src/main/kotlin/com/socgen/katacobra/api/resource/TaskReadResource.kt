package com.socgen.katacobra.api.resource

import com.socgen.katacobra.domaine.task.TaskReadRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/tasks")
class TaskReadResource(private val taskReadRepository: TaskReadRepository) {

    @GetMapping
    fun findAll() = ResponseEntity.ok().body(taskReadRepository.findAll())

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: UUID) = taskReadRepository.findById(id)
            .map { ResponseEntity.ok().body(it) }
            .orElseGet { ResponseEntity.notFound().build() }!!
}