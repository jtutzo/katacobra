package com.socgen.katacobra.api.resource

import com.socgen.katacobra.api.CreateTeamDTO
import com.socgen.katacobra.api.LabelAlreadyUsed
import com.socgen.katacobra.api.UpdateTeamDTO
import com.socgen.katacobra.domaine.CreateTeamCommand
import com.socgen.katacobra.domaine.UpdateTeamCommand
import com.socgen.katacobra.domaine.team.TeamReadRepository
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/teams")
class TeamWriteResource(private val commandGateway: CommandGateway, private val teamReadRepository: TeamReadRepository) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody dto: CreateTeamDTO) {
        if (labelIsAlreadyUsed(dto.label)) throw LabelAlreadyUsed()
        commandGateway.send<CreateTeamCommand>(CreateTeamCommand(supervisorId = dto.supervisorId, label = dto.label, teamType = dto.teamType))
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable("id") id: UUID, @RequestBody dto: UpdateTeamDTO) {
        if (labelIsAlreadyUsed(dto.label)) throw LabelAlreadyUsed()
        commandGateway.send<UpdateTeamCommand>(UpdateTeamCommand(id, dto.supervisorId, dto.label))
    }

    private fun labelIsAlreadyUsed(label: String) = teamReadRepository
            .findByLabel(label)
            .isPresent
}