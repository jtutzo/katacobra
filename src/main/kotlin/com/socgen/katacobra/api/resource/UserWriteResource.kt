package com.socgen.katacobra.api.resource

import com.socgen.katacobra.api.CreateUserDTO
import com.socgen.katacobra.api.UpdateUserDTO
import com.socgen.katacobra.api.UsernameAlreadyUsed
import com.socgen.katacobra.domaine.CreateUserCommand
import com.socgen.katacobra.domaine.UpdateUserCommand
import com.socgen.katacobra.domaine.user.UserReadRepository
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/users")
class UserWriteResource(private val commandGateway: CommandGateway, private val userReadRepository: UserReadRepository) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody dto: CreateUserDTO) {
        if (usernameIsAlreadyUsed(dto.username)) throw UsernameAlreadyUsed()
        commandGateway.send<CreateUserCommand>(CreateUserCommand(username = dto.username, teamId = dto.teamId))
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable("id") id: UUID, @RequestBody dto: UpdateUserDTO) {
        if (usernameIsAlreadyUsed(dto.username)) throw UsernameAlreadyUsed()
        commandGateway.send<UpdateUserCommand>(UpdateUserCommand(id, dto.username, dto.teamId))
    }

    private fun usernameIsAlreadyUsed(username: String) = userReadRepository
            .findByUsername(username)
            .isPresent

}