package com.socgen.katacobra.api.resource

import com.socgen.katacobra.api.CreateTaskDTO
import com.socgen.katacobra.api.LabelAlreadyUsed
import com.socgen.katacobra.api.UpdateTaskDTO
import com.socgen.katacobra.domaine.CreateTaskCommand
import com.socgen.katacobra.domaine.UpdateTaskCommand
import com.socgen.katacobra.domaine.task.TaskReadRepository
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/tasks")
class TaskWriteResource(private val commandGateway: CommandGateway, private val taskReadRepository: TaskReadRepository) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody dto: CreateTaskDTO) {
        if (labelIsAlreadyUsed(dto.label)) throw LabelAlreadyUsed()
        commandGateway.send<CreateTaskCommand>(CreateTaskCommand(label = dto.label))
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable("id") id: UUID, @RequestBody dto: UpdateTaskDTO) {
        if (labelIsAlreadyUsed(dto.label)) throw LabelAlreadyUsed()
        commandGateway.send<UpdateTaskCommand>(UpdateTaskCommand(id, dto.label))
    }

    private fun labelIsAlreadyUsed(label: String) = taskReadRepository.findByLabel(label).isPresent

}
