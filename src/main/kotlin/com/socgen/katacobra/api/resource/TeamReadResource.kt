package com.socgen.katacobra.api.resource

import com.socgen.katacobra.domaine.team.TeamReadRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/teams")
class TeamReadResource(private val teamReadRepository: TeamReadRepository) {

    @GetMapping
    fun findAll() = ResponseEntity.ok().body(teamReadRepository.findAll())

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: UUID) = teamReadRepository.findById(id)
            .map { ResponseEntity.ok().body(it) }
            .orElseGet { ResponseEntity.notFound().build() }!!
}