package com.socgen.katacobra.api

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.CONFLICT)
class LabelAlreadyUsed : RuntimeException("Label is already used.")

@ResponseStatus(HttpStatus.CONFLICT)
class UsernameAlreadyUsed : RuntimeException("Username is already used.")

