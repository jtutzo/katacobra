package com.socgen.katacobra.domaine.user

import com.socgen.katacobra.domaine.UserCreatedEvent
import com.socgen.katacobra.domaine.UserUpdatedEvent
import com.socgen.katacobra.domaine.buildEntity
import org.axonframework.eventhandling.EventHandler
import org.springframework.stereotype.Service

@Service
class UserEventHandler(private val userWriteRepository: UserWriteRepository) {

    @EventHandler
    fun on(evt: UserCreatedEvent) {
        userWriteRepository.create(evt.buildEntity())
    }

    @EventHandler
    fun on(evt: UserUpdatedEvent) {
        userWriteRepository.update(evt.buildEntity())
    }
}