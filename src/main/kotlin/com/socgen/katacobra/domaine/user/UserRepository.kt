package com.socgen.katacobra.domaine.user

import com.socgen.katacobra.domaine.UserEntity
import java.util.*

interface UserReadRepository {

    fun findAll(): Set<UserEntity>

    fun findById(id: UUID): Optional<UserEntity>

    fun findByUsername(username: String): Optional<UserEntity>
}

interface UserWriteRepository {

    fun create(userEntity: UserEntity)

    fun update(userEntity: UserEntity)

    fun deleteAll()
}