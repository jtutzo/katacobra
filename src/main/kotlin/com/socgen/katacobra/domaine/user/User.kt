package com.socgen.katacobra.domaine.user

import com.socgen.katacobra.domaine.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle.apply
import org.axonframework.spring.stereotype.Aggregate
import java.util.*

@Aggregate
class User {

    @AggregateIdentifier
    private var id: UUID? = null

    private var username: String = ""

    private var teamId: UUID? = null

    constructor()

    @CommandHandler
    constructor(cmd: CreateUserCommand) {
        apply(cmd.buildEvent())
    }

    @CommandHandler
    fun handle(cmd: UpdateUserCommand) {
        apply(cmd.buildEvent())
    }

    @EventSourcingHandler
    fun on(evt: UserCreatedEvent) {
        this.id = evt.id
        this.username = evt.username
        this.teamId = evt.teamId
    }

    @EventSourcingHandler
    fun on(evt: UserUpdatedEvent) {
        this.username = evt.username
        this.teamId = evt.teamId
    }
}