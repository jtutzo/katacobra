package com.socgen.katacobra.domaine

import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.util.*

interface Command

data class CreateTaskCommand(val id: UUID? = null,
                             val label: String,
                             val users: Set<UUID> = emptySet(),
                             val teams: Set<UUID> = emptySet()) : Command

data class UpdateTaskCommand(@TargetAggregateIdentifier val id: UUID,
                             val label: String,
                             val users: Set<UUID> = emptySet(),
                             val teams: Set<UUID> = emptySet()) : Command

data class CreateTeamCommand(val id: UUID? = null,
                             val supervisorId: UUID?,
                             val label: String,
                             val teamType: TeamType) : Command

data class UpdateTeamCommand(@TargetAggregateIdentifier val id: UUID,
                             val supervisorId: UUID?,
                             val label: String) : Command

data class CreateUserCommand(val id: UUID? = null,
                             val username: String,
                             val teamId: UUID) : Command

data class UpdateUserCommand(@TargetAggregateIdentifier val id: UUID,
                             val username: String,
                             val teamId: UUID) : Command
