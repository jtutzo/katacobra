package com.socgen.katacobra.domaine.team

import com.socgen.katacobra.domaine.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle.apply
import org.axonframework.spring.stereotype.Aggregate
import java.util.*

@Aggregate
class Team {

    @AggregateIdentifier
    private var id: UUID? = null

    private var supervisorId: UUID? = null

    private var label: String = ""

    private var teamType: TeamType = TeamType.DEFAULT

    constructor()

    @CommandHandler
    constructor(cmd: CreateTeamCommand) {
        apply(cmd.buildEvent())
    }

    @CommandHandler
    fun handle(cmd: UpdateTeamCommand) {
        apply(cmd.buildEvent(this.teamType))
    }

    @EventSourcingHandler
    fun on(evt: TeamCreatedEvent) {
        this.id = evt.id
        this.supervisorId = evt.supervisorId
        this.label = evt.label
        this.teamType = evt.teamType
    }

    @EventSourcingHandler
    fun on(evt: TeamUpdatedEvent) {
        this.supervisorId = evt.supervisorId
        this.label = evt.label
    }
}