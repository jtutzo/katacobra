package com.socgen.katacobra.domaine.team

import com.socgen.katacobra.domaine.TeamEntity
import java.util.*

interface TeamReadRepository {

    fun findAll(): Set<TeamEntity>

    fun findById(id: UUID): Optional<TeamEntity>

    fun findByLabel(label: String): Optional<TeamEntity>
}

interface TeamWriteRepository {

    fun create(team: TeamEntity)

    fun update(Team: TeamEntity)

    fun deleteAll()
}