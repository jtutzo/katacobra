package com.socgen.katacobra.domaine.team

import com.socgen.katacobra.domaine.TeamCreatedEvent
import com.socgen.katacobra.domaine.TeamUpdatedEvent
import com.socgen.katacobra.domaine.buildEntity
import org.axonframework.eventhandling.EventHandler
import org.springframework.stereotype.Service

@Service
class TeamEventHandler(private val teamWriteRepository: TeamWriteRepository) {

    @EventHandler
    fun on(evt: TeamCreatedEvent) {
        teamWriteRepository.create(evt.buildEntity())
    }

    @EventHandler
    fun on(evt: TeamUpdatedEvent) {
        teamWriteRepository.update(evt.buildEntity())
    }
}