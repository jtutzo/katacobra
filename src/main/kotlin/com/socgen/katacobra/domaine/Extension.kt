package com.socgen.katacobra.domaine

import java.util.*

fun CreateTaskCommand.buildEvent() = TaskCreatedEvent(UUID.randomUUID(), this.label)

fun UpdateTaskCommand.buildEvent() = TaskUpdatedEvent(this.id, this.label)

fun TaskCreatedEvent.buildEntity() = TaskEntity(this.id, this.label, this.users, this.teams)

fun TaskUpdatedEvent.buildEntity() = TaskEntity(this.id, this.label, this.users, this.teams)

fun CreateTeamCommand.buildEvent() = TeamCreatedEvent(UUID.randomUUID(), this.supervisorId, this.label, this.teamType)

fun UpdateTeamCommand.buildEvent(teamType: TeamType) = TeamUpdatedEvent(this.id, this.supervisorId, this.label, teamType)

fun TeamCreatedEvent.buildEntity() = TeamEntity(this.id, this.supervisorId, this.label, this.teamType)

fun TeamUpdatedEvent.buildEntity() = TeamEntity(this.id, this.supervisorId, this.label, this.teamType)

fun CreateUserCommand.buildEvent() = UserCreatedEvent(UUID.randomUUID(), this.username, this.teamId)

fun UpdateUserCommand.buildEvent() = UserUpdatedEvent(this.id, this.username, this.teamId)

fun UserCreatedEvent.buildEntity() = UserEntity(this.id, this.username, this.teamId)

fun UserUpdatedEvent.buildEntity() = UserEntity(this.id, this.username, this.teamId)

fun TaskEntity.buildBaseEntity() = BaseTaskEntity(this.id, this.label)
