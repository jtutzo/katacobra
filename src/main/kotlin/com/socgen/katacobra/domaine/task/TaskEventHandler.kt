package com.socgen.katacobra.domaine.task

import com.socgen.katacobra.domaine.TaskCreatedEvent
import com.socgen.katacobra.domaine.TaskUpdatedEvent
import com.socgen.katacobra.domaine.buildEntity
import org.axonframework.eventhandling.EventHandler
import org.springframework.stereotype.Service

@Service
class TaskEventHandler(private val taskWriteRepository: TaskWriteRepository) {

    @EventHandler
    fun on(evt: TaskCreatedEvent) {
        taskWriteRepository.create(evt.buildEntity())
    }

    @EventHandler
    fun on(evt: TaskUpdatedEvent) {
        taskWriteRepository.update(evt.buildEntity())
    }
}