package com.socgen.katacobra.domaine.task

import com.socgen.katacobra.domaine.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle.apply
import org.axonframework.spring.stereotype.Aggregate
import java.util.*

@Aggregate
class Task {

    @AggregateIdentifier
    private var id: UUID? = null

    private var label: String = ""

    private constructor()

    @CommandHandler
    constructor(cmd: CreateTaskCommand) {
        apply(cmd.buildEvent())
    }

    @CommandHandler
    fun handle(cmd: UpdateTaskCommand) {
        apply(cmd.buildEvent())
    }

    @EventSourcingHandler
    fun on(evt: TaskCreatedEvent) {
        this.id = evt.id
        this.label = evt.label
    }

    @EventSourcingHandler
    fun on(evt: TaskUpdatedEvent) {
        this.label = evt.label
    }
}