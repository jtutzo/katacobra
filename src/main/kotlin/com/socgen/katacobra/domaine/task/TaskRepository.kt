package com.socgen.katacobra.domaine.task

import com.socgen.katacobra.domaine.TaskEntity
import java.util.*


interface TaskReadRepository {

    fun findAll(): Collection<TaskEntity>

    fun findById(id: UUID): Optional<TaskEntity>

    fun findByLabel(label: String): Optional<TaskEntity>
}

interface TaskWriteRepository {

    fun create(task: TaskEntity)

    fun update(task: TaskEntity)

    fun deleteAll()
}
