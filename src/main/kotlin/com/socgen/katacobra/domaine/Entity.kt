package com.socgen.katacobra.domaine

import java.util.*

data class BaseTaskEntity(val id: UUID, val label: String)

data class TaskEntity(val id: UUID,
                      val label: String,
                      var users: Set<UUID> = emptySet(),
                      var teams: Set<UUID> = emptySet())

data class TeamEntity(val id: UUID,
                      val supervisorId: UUID?,
                      val label: String,
                      val teamType: TeamType)

data class UserEntity(val id: UUID,
                      val username: String,
                      val teamId: UUID)
