package com.socgen.katacobra.domaine

import java.util.*

interface Event

data class TaskCreatedEvent(val id: UUID,
                            val label: String,
                            val users: Set<UUID> = emptySet(),
                            val teams: Set<UUID> = emptySet()) : Event

data class TaskUpdatedEvent(val id: UUID,
                            val label: String,
                            val users: Set<UUID> = emptySet(),
                            val teams: Set<UUID> = emptySet()) : Event

data class TeamCreatedEvent(val id: UUID,
                            val supervisorId: UUID?,
                            val label: String,
                            val teamType: TeamType) : Event

data class TeamUpdatedEvent(val id: UUID,
                            val supervisorId: UUID?,
                            val label: String,
                            val teamType: TeamType) : Event

data class UserCreatedEvent(val id: UUID,
                            val username: String,
                            val teamId: UUID) : Event

data class UserUpdatedEvent(val id: UUID,
                            val username: String,
                            val teamId: UUID) : Event

