package com.socgen.katacobra.provider.repository

import com.socgen.katacobra.domaine.BaseTaskEntity
import com.socgen.katacobra.domaine.TaskEntity
import com.socgen.katacobra.domaine.buildBaseEntity
import com.socgen.katacobra.domaine.task.TaskReadRepository
import com.socgen.katacobra.domaine.task.TaskWriteRepository
import org.jooq.Condition
import org.jooq.Configuration
import org.jooq.Record4
import org.jooq.Result
import org.jooq.example.db.h2.Tables.*
import org.jooq.example.db.h2.tables.records.TaskRecord
import org.jooq.impl.DAOImpl
import org.jooq.impl.DSL
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*


@Repository
class TaskRepositoryImpl(private val configuration: Configuration) : TaskReadRepository, TaskWriteRepository {

    private val base = object : DAOImpl<TaskRecord, BaseTaskEntity, UUID>(TASK, BaseTaskEntity::class.java, configuration) {
        override fun getId(task: BaseTaskEntity) = task.id
        fun dsl() = super.configuration().dsl()
    }

    override fun findAll() = baseFind(DSL.noCondition())

    override fun findById(id: UUID): Optional<TaskEntity> = Optional.ofNullable(baseFind(TASK.ID.eq(id)).firstOrNull())

    override fun findByLabel(label: String): Optional<TaskEntity> = Optional.ofNullable(baseFind(TASK.LABEL.eq(label)).firstOrNull())

    @Transactional
    override fun create(task: TaskEntity) {
        base.insert(task.buildBaseEntity())
        updateUsers(task.id, task.users)
        updateTeams(task.id, task.teams)
    }

    @Transactional
    override fun update(task: TaskEntity) {
        base.update(task.buildBaseEntity())
        updateUsers(task.id, task.users)
        updateTeams(task.id, task.teams)
    }

    @Transactional
    override fun deleteAll() {
        base.dsl()
                .deleteFrom(TASK_TEAM)
                .execute()

        base.dsl()
                .deleteFrom(TASK_USER)
                .execute()

        base.dsl()
                .delete(TASK)
                .execute()
    }

    private fun baseFind(condition: Condition) = base.dsl()
            .select(
                    TASK.ID,
                    TASK.LABEL,
                    TASK_USER.USER_ID,
                    TASK_TEAM.TEAM_ID)
            .from(TASK
                    .leftJoin(TASK_USER)
                    .on(TASK_USER.TASK_ID.eq(TASK.ID))
                    .leftJoin(TASK_TEAM)
                    .on(TASK_TEAM.TASK_ID.eq(TASK.ID)))
            .where(condition)
            .fetch().buildEntity()

    private fun updateUsers(taskId: UUID, usersId: Set<UUID>) {

        fun delete(taskId: UUID) {
            base.dsl()
                    .deleteFrom(TASK_USER)
                    .where(TASK_USER.TASK_ID.eq(taskId))
                    .execute()
        }

        fun add(taskId: UUID, userId: UUID) {
            base.dsl()
                    .insertInto(TASK_USER)
                    .columns(TASK_USER.ID, TASK_USER.TASK_ID, TASK_USER.USER_ID)
                    .values(UUID.randomUUID(), taskId, userId)
                    .execute()

        }

        delete(taskId)
        usersId.map { add(taskId, it) }

    }

    private fun updateTeams(taskId: UUID, teamsId: Set<UUID>) {

        fun delete(taskId: UUID) {
            base.dsl()
                    .deleteFrom(TASK_TEAM)
                    .where(TASK_TEAM.TASK_ID.eq(taskId))
                    .execute()
        }

        fun add(taskId: UUID, teamId: UUID) {
            base.dsl()
                    .insertInto(TASK_TEAM)
                    .columns(TASK_TEAM.ID, TASK_TEAM.TASK_ID, TASK_TEAM.TEAM_ID)
                    .values(UUID.randomUUID(), taskId, teamId)
                    .execute()
        }

        delete(taskId)
        teamsId.map { add(taskId, it) }

    }

    private fun TaskEntity.addUserIdIfNotNull(usersId: UUID?): TaskEntity {
        this.users = if (usersId != null) this.users.plus(usersId) else this.users
        return this
    }

    private fun TaskEntity.addTeamIdIfNotNull(teamsId: UUID?): TaskEntity {
        this.teams = if (teamsId != null) this.teams.plus(teamsId) else this.teams
        return this
    }

    private fun Result<Record4<UUID, String, UUID, UUID>>.buildEntity() =
            this.fold(mutableMapOf<UUID, TaskEntity>()) { acc, it ->
                acc.getOrPut(it[TASK.ID]) {
                    TaskEntity(
                            id = it[TASK.ID],
                            label = it[TASK.LABEL])
                }
                        .addUserIdIfNotNull(it[TASK_USER.USER_ID])
                        .addTeamIdIfNotNull(it[TASK_TEAM.TEAM_ID])
                acc
            }.map { it.value }.toSet()
}

