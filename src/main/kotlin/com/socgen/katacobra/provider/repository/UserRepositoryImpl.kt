package com.socgen.katacobra.provider.repository

import com.socgen.katacobra.domaine.UserEntity
import com.socgen.katacobra.domaine.user.UserReadRepository
import com.socgen.katacobra.domaine.user.UserWriteRepository
import org.jooq.Configuration
import org.jooq.example.db.h2.Tables.USER
import org.jooq.example.db.h2.tables.records.UserRecord
import org.jooq.impl.DAOImpl
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class UserRepositoryImpl(configuration: Configuration) : UserReadRepository, UserWriteRepository {

    private val base = object : DAOImpl<UserRecord, UserEntity, UUID>(USER, UserEntity::class.java, configuration) {
        override fun getId(user: UserEntity) = user.id
        fun dsl() = configuration().dsl()
    }

    override fun findAll(): Set<UserEntity> = base.findAll().toSet()

    override fun findById(id: UUID): Optional<UserEntity> = Optional.ofNullable(base.findById(id))

    override fun findByUsername(username: String): Optional<UserEntity> = base.dsl()
            .selectFrom(USER)
            .where(USER.USERNAME.eq(username))
            .fetchOptional { UserEntity(it.id, it.username, it.teamId) }

    override fun create(userEntity: UserEntity) = base.insert(userEntity)

    override fun update(userEntity: UserEntity) = base.update(userEntity)

    override fun deleteAll() {
        base.dsl().delete(USER).execute()
    }
}