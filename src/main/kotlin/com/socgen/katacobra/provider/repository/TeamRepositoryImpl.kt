package com.socgen.katacobra.provider.repository

import com.socgen.katacobra.domaine.TeamEntity
import com.socgen.katacobra.domaine.TeamType
import com.socgen.katacobra.domaine.team.TeamReadRepository
import com.socgen.katacobra.domaine.team.TeamWriteRepository
import org.jooq.Configuration
import org.jooq.example.db.h2.Tables.TEAM
import org.jooq.example.db.h2.tables.records.TeamRecord
import org.jooq.impl.DAOImpl
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class TeamRepositoryImpl(configuration: Configuration) : TeamReadRepository, TeamWriteRepository {

    private val base = object : DAOImpl<TeamRecord, TeamEntity, UUID>(TEAM, TeamEntity::class.java, configuration) {
        override fun getId(team: TeamEntity) = team.id
        fun dsl() = this.configuration().dsl()
    }

    override fun findAll(): Set<TeamEntity> = base.findAll().toSet()

    override fun findById(id: UUID): Optional<TeamEntity> = Optional.ofNullable(base.findById(id))

    override fun findByLabel(label: String) = base.dsl()
            .selectFrom(TEAM)
            .where(TEAM.LABEL.eq(label))
            .fetchOptional { TeamEntity(it.id, it.supervisorId, it.label, TeamType.valueOf(it.teamType)) }

    override fun create(team: TeamEntity) = base.insert(team)

    override fun update(team: TeamEntity) = base.update(team)

    override fun deleteAll() {
        base.dsl().delete(TEAM).execute()
    }
}